#1.1 select maker, type from product where type = 'Laptop' order by maker;
#1.2 select model, ram, screen, price from laptop where price >1000 order by ram, price desc; 
#1.3 select * from printer where color = 'y' order by price desc;
#1.4 select model, speed, hd, cd, price from pc where (cd = '12x' or cd='24x') and price <600 order by speed desc;
#1.5 select name, class from ships order by name;
#1.6 select * from pc where speed >=500 and price <800 order by price desc;
#1.7 select * from printer where type != 'Matrix' and price <300 order by type desc;
#1.8 select model, speed from pc where price between 400 and 600 order by hd;
#1.9 select pc.model, pc.speed, pc.hd from pc left join product on pc.model = product.model where product.maker = 'A' order by speed;
#1.10 select model, speed, hd, price from laptop where screen >= 12 order by price desc;
#1.11 select model, type, price from printer where price <300 order by type desc;
#1.12 select model, ram, price from laptop where ram = 64 order by screen;
#1.13 select model, ram, price from pc where ram >64 order by hd;
#1.14 select model, speed, price from pc where speed between 500 and 750 order by hd desc;
#1.15 select * from outcome_o where outcome_o.out >2000 order by date desc;
#1.16 select * from income_o where inc between 5000 and 10000 order by inc;
#1.17 select * from income where point = 1 order by inc;
#1.18 select * from outcome where point = 2 order by outcome.out;
#1.19 select * from classes where country = 'Japan' order by type desc;
#1.20 select name, launched from ships where launched between 1920 and 1942 order by launched desc;
#1.21 select * from outcomes where battle = 'Guadalcanal' order by ship desc;
#1.22 select * from outcomes where result = 'sunk' order by ship desc;
#1.23 select class, displacement from classes where displacement >= 40000 order by type;
#1.24 select trip_no, town_from, town_to from trip where town_from = 'London' or town_to = 'London' order by time_out;
#1.25 select trip_no, town_from, town_to, plane  from trip where plane = 'TU-134' order by time_out desc;
#1.26 select trip_no, town_from, town_to, plane  from trip where plane = 'IL-86' order by plane;
#1.27 select trip_no, town_from, town_to from trip where town_from != 'Rostov' and town_to != 'Rostov' order by plane;

#2.1 select model from pc where model like '%1%1';
#2.2 select * from outcome where month(date) = 3;
#2.3 select * from outcome where day(date) = 14;
#2.4 select name from ships where name rlike '^W[:alpha:]+n$';
#2.5 select name from ships where name like '%e%e';
#2.6 select name, launched from ships where name not rlike 'a$';
#2.7 select name from battles where name like '% %c$' and name not rlike 'c$';
#2.8 select * from trip where (hour(time_out) between 12 and 16) or (hour(time_out)=17 and minute(time_out)=0);
#2.9 select * from trip where (hour(time_in) between 17 and 22) or (hour(time_in)=23 and minute(time_in)=0);
#2.10 select date from pass_in_trip where place like '1%';
#2.11 select date from pass_in_trip where place like '%c';
#2.12 select name from passenger where substring_index(substring_index(name,' ' , 2), ' ', -1) like 'C%';
#2.13 select name from passenger where name not like '% J%';

#3.1 select maker, type, speed, hd from pc join product on pc.model = product.model where hd<=8;
#3.2 select maker from pc join product on pc.model = product.model where speed >=600;
#3.3 select maker from laptop join product on laptop.model = product.model where speed <= 500;
#3.4 select * from laptop join laptop l on laptop.model=l.model where laptop.hd=l.hd and laptop.ram=l.ram and laptop.code!=l.code; 
#3.5 select distinct classes.country, classes.type, classes.class from classes join classes cl on classes.type='bb' and cl.type ='bc' or classes.type='bc' or cl.type = 'bb';
#3.6 select distinct pc.model, maker from pc join product on pc.model = product.model where price <600;
#3.7 select distinct product.model, maker from printer join product on printer.model = product.model where price >300;
#3.8 select maker, pc.model, price from pc join product on pc.model = product.model;
#3.9 select maker, pc.model, price from pc join product on pc.model = product.model;
#3.10 select maker, type, pc.model, speed from pc join product on pc.model = product.model where speed >600;
#3.11 select name, displacement from ships join classes on ships.class=classes.class;
#3.12 select distinct name, date from battles join outcomes on battles.name=outcomes.battle where result = 'OK';
#3.13 select name, country from ships join classes on ships.class=classes.class;
#3.14 select distinct plane, name from trip join company on trip.ID_comp=company.ID_comp where plane = 'Boeing';
#3.15 select distinct name, date from passenger join pass_in_trip on passenger.ID_psg=pass_in_trip.ID_psg;











